package com.guillaumevdn.gslotmachine.migration.v3_0;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import com.guillaumevdn.gcore.GCore;
import com.guillaumevdn.gcore.lib.concurrency.RWHashMap;
import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.data.sql.Query;
import com.guillaumevdn.gcore.lib.file.FileUtils;
import com.guillaumevdn.gcore.lib.migration.BackupBehavior;
import com.guillaumevdn.gcore.lib.migration.Migration;
import com.guillaumevdn.gcore.libs.com.google.gson.Gson;
import com.guillaumevdn.gcore.migration.v8_0.data.InstantMySQL;
import com.guillaumevdn.gslotmachine.GSlotMachine;
import com.guillaumevdn.gslotmachine.data.machine.Machine;
import com.guillaumevdn.gslotmachine.lib.serialization.SerializerGSM;

/**
 * @author GuillaumeVDN
 */
public final class MigrationV3Data extends Migration {

	public MigrationV3Data() {
		super(GSlotMachine.inst(), null, "v2 -> v3 (data)", "data_v3/migrated_v3.0.0_data.DONTREMOVE");
	}

	@Override
	public boolean mustMigrate() {
		return true;  // if not done already
	}

	private final Gson gson = getPlugin().createGsonBuilder().create();
	private final Gson prettyGson = getPlugin().createGsonBuilder().setPrettyPrinting().create();
	private InstantMySQL mysql = null;

	@Override
	protected void doMigrate() throws Throwable {
		YMLConfiguration config = getPlugin().loadConfigurationFile("config.yml");

		// init serializers
		SerializerGSM.init();

		// mysql
		if (config.readString("data_backend.gslotmachine_machines_v3", "JSON").equalsIgnoreCase("MYSQL")) {
			// connect
			attemptOperation("connecting to MySQL", BackupBehavior.NONE, () -> {
				final YMLConfiguration gcoreConfig = GCore.inst().loadConfigurationFile("config.yml");
				final String host = gcoreConfig.readMandatoryString("mysql.host");
				final String name = gcoreConfig.readMandatoryString("mysql.name");
				final String usr = gcoreConfig.readMandatoryString("mysql.machine");
				final String pwd = gcoreConfig.readMandatoryString("mysql.pass");
				final String customArgs = gcoreConfig.readString("mysql.args", "");
				final String url = "jdbc:mysql://" + host + "/" + name + "?allowMultiQueries=true" + customArgs;
				mysql = new InstantMySQL(url, usr, pwd);
			});

			// create tables
			attemptOperation("creating MySQL tables", BackupBehavior.NONE, () -> {
				mysql.performUpdateQuery(getPlugin(), new Query("DROP TABLE IF EXISTS gslotmachine_machines_v3;"
						+ "CREATE TABLE gslotmachine_machines_v3("
						+ "machine_id VARCHAR(50) NOT NULL,"
						+ "data LONGTEXT NOT NULL,"
						+ "PRIMARY KEY(machine_uuid)"
						+ ") ENGINE=InnoDB DEFAULT CHARSET = 'utf8';"));
			});

			// machines
			attemptOperation("converting MySQL machines board", BackupBehavior.NONE, () -> {
				final ResultSet set = mysql.performGetQuery(getPlugin(), new Query("SELECT * FROM gslotmachine_machines"));
				while (set.next()) {
					String id = null;
					try {
						// read
						id = set.getString("id");

						final String type = set.getString("type");
						final Map<String, String> casesRaw = gson.fromJson(set.getString("cases"), new HashMap<String, String>().getClass());
						final RWHashMap<Integer, Location> cases = new RWHashMap<>(5, 1f);
						for (String str : casesRaw.keySet()) {
							cases.put(Integer.parseInt(str), unserializeWXYZLocation(casesRaw.get(str)));
						}
						final Location button = unserializeWXYZLocation(set.getString("button"));
						final Machine machine = new Machine(id, type, cases, button);

						// write
						mysql.performUpdateQuery(getPlugin(), new Query("INSERT INTO gslotmachine_machines_v4 (machine_uuid,data) VALUES (" + Query.escapeValue(id.toString()) + ", " + Query.escapeValue(gson.toJson(machine)) + ");"));
						countMod();
					} catch (Throwable exception) {
						error("Couldn't convert saved machine " + id + ", skipping", exception);
					}
				}
				set.close();
			});

			// close connection
			mysql.close();
		}
		// json
		else {

			// machines
			attemptOperation("converting JSON machines board", BackupBehavior.NONE, () -> {
				File srcMachines = new File(getPluginFolder().getParentFile() + "/GCore_backup_on_v7/data/machine_machines");
				if (!srcMachines.exists()) {  // maybe they've already done the GCoreLegacy conversion a while ago for other plugins, and now we just need GSlotMachine
					srcMachines = new File(getPluginFolder().getParentFile() + "/GCoreLegacy/data/machine_machines");
				}

				final File targetMachines = new File(getPluginFolder() + "/data_v3/machines");
				if (srcMachines.exists()) {
					for (File srcMachine : srcMachines.listFiles()) {
						try {
							final String id = FileUtils.getSimpleName(srcMachine);
							final V3Machine v3 = gson.fromJson(new FileReader(srcMachine), V3Machine.class);

							final RWHashMap<Integer, Location> cases = new RWHashMap<>(5, 1f);
							for (String str : v3.cases.keySet()) {
								cases.put(Integer.parseInt(str), unserializeWXYZLocation(v3.cases.get(str)));
							}
							final Location button = unserializeWXYZLocation(v3.button);
							final Machine machine = new Machine(id, v3.type, cases, button);

							toJson(machine, new File(targetMachines + "/" + machine.getId() + ".json"));
							countMod();
						} catch (Throwable exception) {
							error("Couldn't convert saved machine " + FileUtils.getSimpleName(srcMachine) + ", skipping", exception);
						}
					}
				}
			});
		}
	}

	private void toJson(Object object, File file) throws IOException {
		file.getParentFile().mkdirs();
		FileUtils.reset(file);
		try (FileWriter writer = new FileWriter(file)) {
			prettyGson.toJson(object, writer);
		}
	}

	private static Location unserializeWXYZLocation(String wxyzLocation) {
		if (wxyzLocation == null || wxyzLocation.isEmpty() || wxyzLocation.equals("null")) {
			return null;
		}

		String[] split = wxyzLocation.split(",");
		World world = Bukkit.getWorld(split[0]);

		if (world == null) {
			return null;
		}

		double x = Double.parseDouble(split[1]);
		double y = Double.parseDouble(split[2]);
		double z = Double.parseDouble(split[3]);
		float yaw = 90F;
		float pitch = 0F;

		if (split.length > 4) {
			yaw = Float.parseFloat(split[4]);
			pitch = Float.parseFloat(split[5]);
		}

		return new Location(world, x, y, z, yaw, pitch);
	}

}
