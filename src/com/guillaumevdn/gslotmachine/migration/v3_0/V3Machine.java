package com.guillaumevdn.gslotmachine.migration.v3_0;

import java.util.Map;

public class V3Machine {

	public String id;
	public String type;
	public Map<String, String> cases;
	public String button;

}
