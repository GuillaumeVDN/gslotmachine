package com.guillaumevdn.gslotmachine;

import com.guillaumevdn.gcore.lib.string.TextElement;
import com.guillaumevdn.gcore.lib.string.TextEnumElement;

/**
 * @author GuillaumeVDN
 */
public enum TextGSM implements TextEnumElement {

	commandDescriptionGslotmachineEdit,

	commandDescriptionGslotmachineCreateMachine,
	commandDescriptionGslotmachineEditMachine,
	commandDescriptionGslotmachineDeleteMachine,
	commandDescriptionGslotmachineListMachines

	;

	private TextElement text = new TextElement();

	TextGSM() {
	}

	@Override
	public String getId() {
		return name();
	}

	@Override
	public TextElement getText() {
		return text;
	}

}
