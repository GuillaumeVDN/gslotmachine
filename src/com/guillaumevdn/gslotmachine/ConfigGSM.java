package com.guillaumevdn.gslotmachine;

import java.util.List;

import com.guillaumevdn.gcore.lib.GPluginConfig;
import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.collection.SortedHashMap;
import com.guillaumevdn.gcore.lib.compatibility.particle.Particle;
import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.element.type.container.ElementNotify;
import com.guillaumevdn.gslotmachine.lib.machine.element.MachineTypesContainer;

/**
 * @author GuillaumeVDN
 */
public class ConfigGSM extends GPluginConfig {

	public static List<String> commandsAliasesEdit;
	public static List<String> commandsAliasesCreateMachine;
	public static List<String> commandsAliasesEditMachine;
	public static List<String> commandsAliasesDeleteMachine;
	public static List<String> commandsAliasesListMachines;

	public static int totalDurationTicks;
	public static int finalFreezeDurationTicks;
	public static int initialDelayTicks;
	public static SortedHashMap<Integer, Integer> changeDelays;  // high to low

	public static List<Particle> caseParticles;

	public static ElementNotify notifyWin;
	public static ElementNotify notifyWinBroadcast;
	public static ElementNotify notifyLose;

	public static MachineTypesContainer machineTypes;

	@Override
	protected YMLConfiguration doLoad() throws Throwable {
		final YMLConfiguration baseConfig = GSlotMachine.inst().loadConfigurationFile("config.yml");

		commandsAliasesEdit = CollectionUtils.asLowercaseList(baseConfig.readMandatoryStringList("commands_aliases.edit"));
		commandsAliasesCreateMachine = CollectionUtils.asLowercaseList(baseConfig.readMandatoryStringList("commands_aliases.createmachine"));
		commandsAliasesEditMachine = CollectionUtils.asLowercaseList(baseConfig.readMandatoryStringList("commands_aliases.editmachine"));
		commandsAliasesDeleteMachine = CollectionUtils.asLowercaseList(baseConfig.readMandatoryStringList("commands_aliases.deletemachine"));
		commandsAliasesListMachines = CollectionUtils.asLowercaseList(baseConfig.readMandatoryStringList("commands_aliases.listmachines"));

		totalDurationTicks = (int) (baseConfig.readDurationMillis("total_duration") / 50L);
		finalFreezeDurationTicks = (int) (baseConfig.readDurationMillis("final_freeze_duration") / 50L);
		initialDelayTicks = (int) (baseConfig.readDurationMillis("initial_delay") / 50L);
		changeDelays = SortedHashMap.keySortedReverse();
		for (String key : baseConfig.readKeysForSection("change_delays")) {
			final int after = (int) (baseConfig.readDurationMillis("change_delays." + key + ".after") / 50L);
			final int delay = (int) (baseConfig.readDurationMillis("change_delays." + key + ".delay") / 50L);
			changeDelays.put(after, delay);
		}

		caseParticles = baseConfig.readMandatorySerializedList("case_particles", Particle.class);

		notifyWin = baseConfig.readNotify("notify_win");
		notifyWinBroadcast = baseConfig.readNotify("notify_win_broadcast");
		notifyLose = baseConfig.readNotify("notify_lose");

		(machineTypes = new MachineTypesContainer()).load();

		return baseConfig;
	}

}
