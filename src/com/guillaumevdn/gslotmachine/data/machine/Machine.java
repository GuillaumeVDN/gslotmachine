package com.guillaumevdn.gslotmachine.data.machine;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;

import com.guillaumevdn.gcore.lib.concurrency.RWHashMap;
import com.guillaumevdn.gslotmachine.ConfigGSM;
import com.guillaumevdn.gslotmachine.lib.machine.active.ActiveMachine;
import com.guillaumevdn.gslotmachine.lib.machine.element.ElementMachineType;

/**
 * @author GuillaumeVDN
 */
public final class Machine {

	private final String id;
	private String type = null;
	private RWHashMap<Integer, Location> cases = new RWHashMap<>(5, 1f);
	private Location button;

	private transient ActiveMachine active = null;

	public Machine(String id) {
		this.id = id;
	}

	public Machine(String id, String type, RWHashMap<Integer, Location> cases, Location button) {
		this.id = id;
		this.type = type;
		this.cases = cases;
		this.button = button;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public ElementMachineType getTypeElement() {
		return ConfigGSM.machineTypes.getElement(type).orNull();
	}

	public void setType(ElementMachineType type) {
		this.type = type.getId();
		setToSave();
	}

	public RWHashMap<Integer, Location> getCases() {
		return cases;
	}

	public Location getCase(int number) {
		return cases.get(number);
	}

	public void setCase(int number, Location loc) {
		cases.put(number, loc);
		setToSave();
	}

	public void removeCase(int number) {
		cases.remove(number);
		setToSave();
	}

	public Location getButton() {
		return button;
	}

	public void setButton(Location button) {
		this.button = button;
		setToSave();
	}

	public ActiveMachine getActive() {
		return active;
	}

	public void setActive(ActiveMachine active) {
		this.active = active;
	}

	public boolean isComplete() {
		return getTypeElement() != null && !cases.isEmpty() && areCasesCorrect() && button != null;
	}

	private boolean areCasesCorrect() {
		int last = 0;
		for (int nb : cases.streamResultKeys(s -> s.sorted().collect(Collectors.toList()))) {
			if (nb != last + 1) {
				return false;
			}
			last = nb;
		}
		return true;
	}

	public void sendNextStepMessage(CommandSender sender) {
		String step = null;
		if (type == null) {
			step = "§a/machine em " + id + " settype [type]";
		} else if (button == null) {
			step = "§a/machine em " + id + " setbutton";
		} else if (cases.isEmpty()) {
			step = "§a/machine em " + id + " setcase 1";
		} else if (!areCasesCorrect()) {
			int last = 0;
			for (int nb : cases.streamResultKeys(s -> s.sorted().collect(Collectors.toList()))) {
				if (nb != last + 1) {
					break;
				}
				last = nb;
			}
			step = "§a/machine em " + id + " setcase " + (last + 1);
		}
		if (step != null) {
			sender.sendMessage("§7Next step : " + step);
		}
	}

	// ----- data / cached operations / process / fetch / etc

	public void setToSave() {
		BoardMachines.inst().addCachedToSave(id);
	}

	public static Machine cachedOrNull(String key) { return BoardMachines.inst().getCachedValue(key); }
	public static void forCached(String key, Consumer<Machine> ifCached) { Machine user = cachedOrNull(key); if (user != null) { ifCached.accept(user); } }
	public static <T> T forCached(String key, Function<Machine, T> ifCached, T def) { Machine user = cachedOrNull(key); return user == null ? def : ifCached.apply(user); }

}
