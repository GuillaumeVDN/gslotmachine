package com.guillaumevdn.gslotmachine.lib.serialization.adapter;

import org.bukkit.Location;

import com.guillaumevdn.gcore.lib.concurrency.RWHashMap;
import com.guillaumevdn.gcore.lib.serialization.adapter.DataAdapter;
import com.guillaumevdn.gcore.lib.serialization.data.DataIO;
import com.guillaumevdn.gslotmachine.data.machine.Machine;

/**
 * @author GuillaumeVDN
 */
public final class AdapterMachine extends DataAdapter<Machine> {

	public static final AdapterMachine INSTANCE = new AdapterMachine();

	private AdapterMachine() {
		super(Machine.class, 1);
	}

	@Override
	public void write(Machine machine, DataIO writer) throws Throwable {
		writer.write("id", machine.getId());
		writer.write("type", machine.getType());
		writer.writeObject("cases", mapWriter -> {
			machine.getCases().forEach((nb, loc) -> {
				mapWriter.write("" + nb, loc);
			});
		});
		writer.write("button", machine.getButton());
	}

	@Override
	public Machine read(int version, DataIO reader) throws Throwable {
		if (version == 1) {
			final String id = reader.readString("id");
			final String type = reader.readString("type");
			final RWHashMap<Integer, Location> cases = reader.readSimpleMap("cases", Integer.class, new RWHashMap<Integer, Location>(5, 1f),
					(key, __, r) -> r.readSerialized(key, Location.class)
					);
			final Location button = reader.readSerialized("button", Location.class);

			return new Machine(id, type, cases, button);
		}
		throw new IllegalArgumentException("unknown adapter version " + version);
	}

}
